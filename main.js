// Task 1
for (let i=1; i <= 100; i++){
    if (i % 15 == 0) 
    document.write("FizzBuzz" + '<br>');
    else if (i % 3 == 0) 
    document.write("Fizz" + '<br>');
    else if (i % 5 == 0) 
    document.write("Buzz"  + '<br>');
    else document.write(i  + '<br>');
}

document.write('<br>' + '<br>')//just empty space divider

// Task 2
let repl = /,/gi;
let string = 'Лето удалось очень жарким, было много ягод и фруктов, лучшим местом для отдыха была речка.';
let newString = string.replace(repl, '.');
newString = newString.replace(/(^|\.\s+)(.)/g, function(a, b, c){
    return b + c.toUpperCase();
  });
document.write(newString);

document.write('<br>' + '<br>')//just empty space divider

// Task 3
let strBday = 'Антон, Дима и Женя пришли на День рождения сразу, а Миша пришел позже';
let separators = [' |, '];
let array = strBday.split(new RegExp(separators.join('|'), 'g'));
let nameAn = array[0];
let nameDi = array[1];
let nameZh = array[3];
let nameMi = array[10];
let strNew = 'На день рождения пришли:' + ' ' + nameAn + ', ' + nameDi + ', ' + nameZh + ', ' + nameMi;
document.write(strNew);
